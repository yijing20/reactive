package com.example.sr_c1_e1.repositories;

import com.example.sr_c1_e1.model.Stocks;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Repository
public interface StockRepository extends ReactiveCrudRepository<Stocks, Integer> {
    Mono<Stocks> findByProdId(int prodId);

    Flux<Stocks> findByProdIdIn(List<Integer> prodIds);
}