package com.example.sr_c1_e1.repositories;

import com.example.sr_c1_e1.model.Products;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Repository
public interface ProductRepository extends ReactiveCrudRepository<Products, Integer> {
    Mono<Products> findById(int prodId);

    Flux<Products> findAll();
    Flux<Products> findByIdIn(List<Integer> prodIds);
}
