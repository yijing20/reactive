package com.example.sr_c1_e1.repositories;

import com.example.sr_c1_e1.model.Details;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Repository
public interface DetailsRepository extends ReactiveCrudRepository<Details, Integer> {
    Mono<Details> findByProdId(int prodId);

    Flux<Details> findByProdIdIn(List<Integer> prodIds);
}
