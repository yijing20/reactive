package com.example.sr_c1_e1.model;

public class ProductDetails {

    private int id;
    private String name;
    private String description;
    private Double price;
    private int stock;

    public ProductDetails(Products product, Details details, Stocks stock) {
        this.id = product.getId();
        this.name = product.getName();
        this.description = details.getDescription();
        this.price = details.getPrice();
        this.stock = stock.getQty();
    }

//    public ProductDetails(Products product, Details detailsObj) {
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
