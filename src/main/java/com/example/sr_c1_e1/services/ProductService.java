package com.example.sr_c1_e1.services;

import com.example.sr_c1_e1.model.Details;
import com.example.sr_c1_e1.model.ProductDetails;
import com.example.sr_c1_e1.model.Products;
import com.example.sr_c1_e1.model.Stocks;
import com.example.sr_c1_e1.repositories.DetailsRepository;
import com.example.sr_c1_e1.repositories.ProductRepository;
import com.example.sr_c1_e1.repositories.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.List;

@Service
public class ProductService {

  private final ProductRepository productRepository;
//  @Autowired
  private final DetailsRepository detailsRepository;
//  @Autowired
  private final StockRepository stockRepository;


  @Autowired
  public ProductService(ProductRepository productRepository, DetailsRepository detailsRepository, StockRepository stockRepository) {
    this.productRepository = productRepository;
    this.detailsRepository = detailsRepository;
    this.stockRepository = stockRepository;
  }

  /**
   FLUX
   **/
  public Flux<Products> getProducts() { // whole method takes 10 seconds to execute
    return productRepository.findAll(); // 2 products
//        .delayElements(Duration.ofSeconds(5)); // for any element add a sleep duration
  }

  public Flux<Products> getFProductById(List<Integer> productIds) {
    return Flux.fromIterable(productIds)
            .flatMap(productId -> productRepository.findById(productId))
            .onErrorResume(error -> Flux.error(new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error occurred while fetching products", error)));
  }

  public Flux<ProductDetails> getProductDetailsById(Integer id) {
    Flux<Products> productsMono = productRepository.findByIdIn(Collections.singletonList(id));
    Flux<Details> detailsFlux = detailsRepository.findByProdIdIn(Collections.singletonList(id));
    Flux<Stocks> stocksMono = stockRepository.findByProdIdIn(Collections.singletonList(id));

    return Flux.zip(
            Flux.from(productsMono),
            detailsFlux.collectList(),
            stocksMono
    ).flatMap(tuple -> {
      Products product = tuple.getT1();
      List<Details> details = tuple.getT2();
      Stocks stocks = tuple.getT3();

      Flux<ProductDetails> productDetailsFlux = Flux.fromIterable(details)
              .map(detailsObj -> new ProductDetails(product, detailsObj, stocks))
              .zipWith(Mono.just(stocks), (pd, s) -> {
                pd.setStock(s.getQty());
                return pd;
              });

      return productDetailsFlux;
    });
  }


  /**
   MONO
   **/
  public Mono<Products> getProductById(int productId){
    return productRepository.findById(productId);
  }

  public Mono<ProductDetails> getProductDetailsById(int productId) {
    return productRepository.findById(productId)
            .flatMap(product -> Mono.zip(
                                    detailsRepository.findByProdId(product.getId()),
                                    stockRepository.findByProdId(product.getId())
                            ).map(tuple -> new ProductDetails(product, tuple.getT1(), tuple.getT2()))
                            .switchIfEmpty(Mono.error(new ChangeSetPersister.NotFoundException()))
            )
            .switchIfEmpty(Mono.error(new ChangeSetPersister.NotFoundException()));
  }


  //ANOTHER WAY TO GET PRODUCT DETAILS BY ID
//  public Mono<ProductDetails> getProductDetailsById(int productId) {
//    System.out.println("Fetching product by ID: " + productId); // log that a product is being fetched
//    Mono<Products> productMono = productRepository.findById(productId);
//    productMono.subscribe(product -> System.out.println("Found product: " + product)); // log the product that was found
//
//    System.out.println("Fetching details for product with ID: " + productId);
//    Mono<Details> detailsMono = productMono.flatMap(products -> detailsRepository.findByProdId(products.getId()));
//    detailsMono.subscribe(details -> System.out.println("Found details: " + details));
//
//    System.out.println("Fetching stock for product with ID: " + productId);
//    Mono<Stocks> stockMono = productMono.flatMap(products -> stockRepository.findByProdId(products.getId()));
//    stockMono.subscribe(stock -> System.out.println("Found stock: " + stock));
//
//    return Mono.zip(productMono, detailsMono, stockMono)
//            .map(tuple -> new ProductDetails(tuple.getT1(), tuple.getT2(), tuple.getT3()))
//            .onErrorMap(e -> new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error occurred while fetching product details", e));
//  }

}
