package com.example.sr_c1_e1.controllers;

import com.example.sr_c1_e1.model.ProductDetails;
import com.example.sr_c1_e1.model.Products;
import com.example.sr_c1_e1.services.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductController {

  private final ProductService productService;
  public ProductController(ProductService productService) {
    this.productService = productService;
  }

  /**
   FLUX - Get all data from 'products'
   **/
  @GetMapping(value = "/flux-products", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
  public Flux<Products> getProduct() {
    return productService.getProducts();
  }

  /**
   FLUX - Get data from 'products' by id
   **/
  @GetMapping(value = "/flux-products/{id}")
  public Flux<Products> getFProductById(@PathVariable Integer id) {
    List<Integer> idList = new ArrayList<>();
    idList.add(id);
    return productService.getFProductById(idList);
  }

  /**
   FLUX - Get data from 'products, details and stocks' by id
   **/
  @GetMapping(value = "/flux-products-details/{id}")
  public Flux<ProductDetails> getProductDetailsById(@PathVariable Integer id){
      return productService.getProductDetailsById(id);
  }


  /**
   MONO - Get data from 'products' by id
   **/
  @GetMapping(value = "/mono-products/{id}")
  public Mono<Products> getMProduct(@PathVariable int id){
    return productService.getProductById(id);
  }

  /**
   MONO - Get data from 'products, details and stocks' by prod_id
   **/
  @GetMapping(value = "/mono-products-details/{id}")
  public Mono<ProductDetails> getProductDetails(@PathVariable int id){
    return productService.getProductDetailsById(id)
            .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "Product not found")))
            .onErrorResume(error -> Mono.error(new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error occurred while fetching product details")));
  }



  /** TESTING **/



}
